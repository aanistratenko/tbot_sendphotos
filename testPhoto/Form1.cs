﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace testPhoto
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			string photoPath = textBox1.Text;
			Image img = Image.FromFile(photoPath);

			byte[] test = ImageToByte(img);
			SendPhoto(test);

		}

		private byte[] ImageToByte(System.Drawing.Image image)
		{
			using (var ms = new MemoryStream())
			{
				image.Save(ms, image.RawFormat);
				return ms.ToArray();
			}
		}

		private void SendPhoto(byte[] inpByte)
		{
			string botId = tbBotId.Text;
			string chatId = tbChatID.Text;
			string url = "https://api.telegram.org/" + botId + "/sendPhoto?chat_id=" + chatId;
			string boundary = "----------------------------------------" + DateTime.Now.Ticks.ToString("x");
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

			request.Method = "POST";
			request.ContentType = "multipart/form-data; boundary=" + boundary;
			request.KeepAlive = true;

			Stream stream = new MemoryStream();
			var boundaryBytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
			var endBoundaryBytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "--");
			string formTemplate = "\r\n--" + boundary + "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";
			string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n" + "Content-Type: application/octet-stream\r\n\r\n";

			stream.Write(boundaryBytes, 0, boundaryBytes.Length);

			var header = string.Format(headerTemplate, "photo", "photo.jpg");
			var headerBytes = Encoding.UTF8.GetBytes(header);

			stream.Write(headerBytes, 0, headerBytes.Length);

			
			stream.Write(inpByte, 0, inpByte.Length);

			stream.Write(endBoundaryBytes, 0, endBoundaryBytes.Length);

			request.ContentLength = stream.Length;

			using (Stream requestStream = request.GetRequestStream())
			{
				stream.Position = 0;
				byte[] tempBuffer = new byte[stream.Length];
				stream.Read(tempBuffer, 0, tempBuffer.Length);
				stream.Close();
				requestStream.Write(tempBuffer, 0, tempBuffer.Length);
			}

			try
			{
				var sendResponse = (HttpWebResponse)request.GetResponse();
				var resp = new StreamReader(sendResponse.GetResponseStream()).ReadToEnd();
				if (sendResponse.StatusCode == HttpStatusCode.OK)
				{
					MessageBox.Show("OK");
				}
			}
			catch (WebException err){}
		}
	}
}
